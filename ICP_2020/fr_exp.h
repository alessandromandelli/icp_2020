#ifndef Z_FR_EXP_H

#define Z_FR_EXP_H


       // ****************************************
       // Initialize COM port (RS-232, USB or LAN)
       // ****************************************
enum BAUD_RATES {
  BAUD_38K      = 0, // ***don't change***
  BAUD_115K     = 1,
  BAUD_230K     = 2,
  BAUD_460K     = 3,
  BAUD_921K     = 4,
  BAUD_LAST
};
       // ***********
       // Action List
       // ***********

enum ACTION_LIST {
  ACT_PROG = 1, //programming
  ACT_VER  = 2, //verification
  ACT_READ = 3, //read
  ACT_BC   = 4, //blank check

  ACT_STA_PROG       = 5, //standalone programming
  ACT_STA_GET_RES    = 6, //standalone: comminicate and get all results
  ACT_STA_CLR_RES    = 7, //standalone: comminicate and clear all results
  ACT_STA_START_PROG = 8, //only start standalone programming

};
       // *************
       // Memory Spaces
       // *************
enum MEMORY_SPACES {
  PM_SPACE  = 0x0001, //program memory
  ID_SPACE  = 0x0002, //ID locations
  DM_SPACE  = 0x0004, //data memory
  CM_SPACE  = 0x0008, //calibration memory (not supported)
  FU_SPACE  = 0x0010, //configuration word
  BM_SPACE  = 0x0020, //boot memory
  OTP_SPACE = 0x0040, //OTP memory

  ALL_SPACE = PM_SPACE | ID_SPACE | DM_SPACE | FU_SPACE | BM_SPACE, //excluding OTP

  LAST_SPACE = 0x0080
};

       // ***************
       // Returned Errors
       // ***************
enum AUTO_ERROR_LEVEL { //return values
       AUTO_OK            = 0,  //operation OK
       AUTO_DB_ERR        = 1,  //data base error
       AUTO_COM_ERR       = 2,  //communication error
       AUTO_VDD_ERR       = 3,  //Vdd overload error
       AUTO_VPP_ERR       = 4,  //Vpp overload error
       AUTO_HEX_ERR       = 5,  //HEX file loading error
       AUTO_SER_ERR       = 6,  //serialization file error
       AUTO_VER_ERR       = 7,  //verification error
       AUTO_ERR_NO_SPACE  = 8,  //no space selected
       AUTO_SAVE_ERR      = 9,  //file save error
       AUTO_SOCK_ERR      = 10, //socket communication error (obsolete)
       AUTO_I2C_ERR       = 11, //Connection error with UUT or GANG/COMBO address conflict
       AUTO_DLL_ERR       = 12, //DLL programming is not supported
       AUTO_KEY_ERR       = 13, //key generation error
       AUTO_CFG_ERR       = 14, //config. file error
       AUTO_COM_NUM_ERR   = 15, //invalid COM number
       AUTO_COM_BUSY_ERR  = 16, //selected COM is busy
       AUTO_COM_BAUD_ERR  = 17, //invalid baud rate
       AUTO_COM_NO_OPEN   = 18, //can't open COM port
       AUTO_USER_CANCEL   = 19, //user cancel
       AUTO_IN_PROGRESS   = 20, //operation in progress
       AUTO_BC_ERR        = 21, //blank check error
       AUTO_OP_NOT_ALLOW  = 22, //operation not allowed for selected programmer
       AUTO_FW_INVALID    = 23, //firmware invalid-firmware upgrade needed
       AUTO_24LC_ADDR_ERR = 24, //24LC01 address (offset) is out of range
       AUTO_DM_ADDR_ERR   = 25, //DM range error
       AUTO_FIRM_ERR      = 26, //firmware version error
       AUTO_NO_SUB        = 27, //no ICP-SUB PCB
       AUTO_NO_SUP_KEE    = 28, //no keeloq support
       AUTO_NO_SUP_DSPIC  = 29, //no dsPIC/PIC24 support
       AUTO_ICP2_REQ      = 30, //ICP2 required
       AUTO_DEV_ERR       = 31, //device selection error (unspecified error)
       AUTO_PROG_MISMATCH = 32, //mismatch between selected and detected programmers
       AUTO_PRJ_INVALID   = 33, //Invalid environment
       AUTO_PRJ_DB_FIRM_PC_MIS   = 34, //mismatch between PC and firmware database
       AUTO_PRJ_DB_FIRM_AT45_MIS = 35, //mismatch between project and firmware database
       AUTO_DLL_SUPPORT_REQIURED = 36, //"GO" pressed on hardware and no DLL support
       AUTO_PRJ_CS        = 37, //project CS error
       AUTO_STA_IDLE      = 38, //programmer is idle or standalone operation can't be started
       AUTO_STA_BUSY      = 39, //standalone operation: programmer busy
       AUTO_ENV_ERR       = 40, //environment (project) file error
       AUTO_PM_RANGE      = 41, //invalid PM range specified
       AUTO_SEC_SUPPORT_REQUIRED = 42, //Security support required
       AUTO_SEC_CNT_INTEG        = 43, //Security feature: integrity error in counter
       AUTO_SEC_CNT_ZERO         = 44, //Security feature: counter = 0
       AUTO_SEC_NO_FUNC          = 45, //Security feature: function does not exist
       AUTO_SEC_PACK_ERR         = 46, //Security feature: packet error
       AUTO_SEC_EEPROM_FAIL      = 47, //Security feature: EEPROM error
       AUTO_SEC_ANTI_SCAN        = 48, //Security feature: anti-scan activated,
       AUTO_SEC_SEC_ID_CMP       = 49, //Security feature: incorrect Security ID
       AUTO_SEC_PASSW_CMP        = 50, //Security feature: incorrect password
       AUTO_SEC_BATCH_CMP        = 51, //Security feature: incorrect batch
       AUTO_SEC_VERS_ERR         = 52, //Security feature: version error
       AUTO_SEC_UNKNOWN_ERR      = 53, //Security feature: unknown error
       AUTO_NO_ROW_ERASE         = 54, //row erase is not supported
       AUTO_INVALID_PARAM        = 55, //invalid parameters
       AUTO_MOVLW_RETLW_CALIB    = 56, //no movlw in calibration word
       AUTO_NO_USUAL_ENV_TRAN    = 57, //Usual environment can't be sent
                                       //if a secure one inside
       AUTO_SEC_BUF_START_ADDR    = 58, //sec. buf. properties error: incorrect start addr
       AUTO_SEC_BUF_END_ADDR      = 59, //sec. buf. properties error: incorrect end addr
       AUTO_SEC_BUF_PAGE_START    = 60, //sec. buf. properties error: incorrect page start
       AUTO_SEC_BUF_PAGE_SIZE     = 61, //sec. buf. properties error: incorrect page size
       AUTO_SEC_BUF_NOT_EVEN      = 62, //sec. buf. properties error: length not even
       AUTO_SEC_BUF_NO_DM         = 63, //sec. buf. properties error: no DM in PIC
       AUTO_SEC_BUF_LAST_PAGE     = 64, //sec. buf. properties error: last PM page can't be used
       AUTO_SEC_BUF_NO_16BIT_SUP  = 65, //sec. buf. properties error: no Script 1 for 16-bit devices
       AUTO_SEC_BUF_NOT_MODULO_3  = 66, //sec. buf. properties error: length not modulo 3

       AUTO_SEC_EMPTY_MASK        = 67, //Security feature: empty mask for secure environment

       AUTO_TEST_COM_NO_SUPPORT   = 68, //ICP2 test command not supported
       AUTO_TEST_NACK             = 69, //ICP2 test command returns NACK

       AUTO_NO_SUP_P32            = 70, //no 32-bit support
       AUTO_PIC32_BUSY_OR_DAMAGED = 71, //PIC32 is busy or damaged
       AUTO_PIC32_CP_OR_DAMAGED   = 72, //PIC32 is code protected or damaged
       AUTO_PIC32_PE_ANSWER       = 73, //PIC32 programming executive: no answer
       AUTO_PIC32_PE_VERSION      = 74, //obsolete: PIC32 programming executive: incorrect version
       AUTO_SEC_BUF_NO_32BIT_SUP  = 75, //no security support for PIC32

       AUTO_CNT_ZERO              = 76, //non-secure (low-endurance) counter is 0
       AUTO_SQTP_CONFLICT         = 77, //serialization from PC is not allowed if standalone serialization=ON

       AUTO_INVALID_DEVICE_CFG    = 78, //invalid device number in CFG file. Use latest DLL
       AUTO_DEV_ID_NO_SUPPORT     = 79, //Device ID read is not supported for the family

       AUTO_ROW_PM_RANGE          = 80, //invalid PM range due to row size

       AUTO_PE_MISMATCH           = 81, //obsolete: Programming executive: mismatch between
                                        //environment and firmware
       AUTO_PE_NO_PGD_PULLDOWN    = 82, //No pull-down on PGD line
       AUTO_PE_VER                = 83, //PE verification failed
       AUTO_PE_NO_IN_ENV          = 84, //PE does not present in environment
       AUTO_PE_CALIB              = 85, //invalid calibration/diagnostic data

       AUTO_PC_DRV_STA_CONFLICT   = 86, //conflict between PC-driven and standalone modes

       AUTO_CALIB_WORD_1_CORRUPT  = 87, //Calibration word 1 corrupted during programming
       AUTO_CALIB_WORD_2_CORRUPT  = 88, //Calibration word 2 corrupted during programming

       AUTO_ENV_NUM_OUT_RANGE     = 89, //Specified environment number is out of range

       AUTO_CYBL_ACQUIRE_TIMEOUT  = 90, //Device acquire timeout
       AUTO_CYBL_SROM_ACT_TIMEOUT = 91, //SROM operation timeout
       AUTO_CYBL_VIRGIN_DEVICE    = 92, //Device is VIRGIN

       AUTO_CYBL_SWD_ACK_FAULT    = 93, //ACK response for SWD transfer is not OK (old name)
       AUTO_SWD_ACK_FAULT         = 93, //ACK response for SWD transfer is not OK (new name)

       AUTO_NO_FIRMWARE_CYBL      = 94, //no firmware for CYBL10x6x
       AUTO_NO_FIRMWARE_I2C       = 95, //no firmware for I2C
       AUTO_NO_FIRMWARE_DSPIC     = 96, //no firmware for dsPIC
       AUTO_NO_FIRMWARE_P32       = 97, //no firmware for PIC32

       AUTO_G3_REQUIRED           = 98, //G3 hardware is required for selected device
       AUTO_G3_NO_PIC17C          = 99, //PIC17C is not supported by G3

       AUTO_RESERVED_100          = 100, //reserved
       AUTO_DEMO_ERR              = 101, //demo version

       AUTO_OTP_NOT_BLANK         = 102, //OTP area is not blank, no programming is allowed
       AUTO_OTP_VER_ERR           = 103, //OTP verification error
       AUTO_FBOOT_VER_ERR         = 104, //FBOOT verification error
       AUTO_DUAL_PART_ILLEGAL_BUF = 105, //illegal partition mode in programming buffer
       AUTO_DUAL_PART_MISMATCH    = 106, //partition mode mismatch
       AUTO_NOT_ALLOWED_IN_DUAL   = 107, //operation is not allowed in dual partition mode
       AUTO_DUAL_PART_ILLEGAL_PIC = 108, //illegal partition mode in PIC
       AUTO_FBOOT_BLANK_ERR       = 109, //FBOOT blank check error
       AUTO_ENV_SIZE_ERR          = 110, //environment size is too big for connected programmer
       AUTO_GANG_COMBO_MISMATCH   = 111, //mismatch between GANG and COMBO

       AUTO_SWD_DEVICE_PROTECTED  = 112, //SWD device is protected
       AUTO_DEVICE_PROTECTED      = 112, //Device is protected (same errorcode as for AUTO_SWD_DEVICE_PROTECTED)

       AUTO_SECURITY_BIT_VER_ERR  = 113, //Security bit verification error

       AUTO_CANT_CONNECT_TO_UUT   = 114, //can't connect to UUT (target)
       AUTO_SINGLE_WORD_RD_NO_SUP = 115, //single word read not supported
       AUTO_SINGLE_WORD_WR_NO_SUP = 116, //single word write not supported
       AUTO_ERASE_WRITE_TIMEOUT   = 117, //erase or write timeout

       AUTO_UPDI_TINY_CRC_FAULT   = 118, //CRC fail. Execute programming with enabled bulk erase
       AUTO_CANT_CONNECT_TO_UPDI  = 119, //can't connect to UPDI UUT (target)

       AUTO_CANT_CONNECT_TO_TPI   = 120, //can't connect to TPI UUT (target)
       AUTO_FTDI_LATENCY_BIG      = 121, //FTDI latency is too big

       AUTO_FWU_CONS_INVALID        = 122, //Unexpected error: Cons.Valid is false
       AUTO_FWU_VERSION_3_2         = 123, //Firmware version must be 3.2 or greater
       AUTO_FWU_SELECTDEVICE        = 124, //Select Device error
       AUTO_FWU_NOT_FTB9            = 125, //Hex file error: not FTB9 firmware file
       AUTO_FWU_NOT_G3              = 126, //Hex file error: not G3 firmware file
       AUTO_FWU_NOT_ICP2            = 127, //Hex file error: not ICP2 firmware file
       AUTO_FWU_NOT_ICP01           = 128, //Hex file error: not ICP-01 firmware file
       AUTO_FWU_CHECKSUM            = 129, //Hex file checksum error
       AUTO_FWU_NOT_PORT_ACTIVATION = 130, //Selected HEX file is not a file for activation of ICP2-Portable options
       AUTO_FWU_NOT_ACTIVATION      = 131, //Selected HEX file is not a file for activation of options
       AUTO_FWU_VERSION_14_2        = 132, //Firmware 14.2 is not allowed
       AUTO_FWU_VERSION_14_0        = 133, //14.0 is the minimum firmware version for ICP2-Portable
       AUTO_FWU_INVALIDFILE         = 134, //Selected HEX file is not a file for firmware upgrade
       AUTO_FWU_VERSION_UNKNOWN     = 135, //Unknown firmware version

       AUTO_STM32L1_ERRATA_PCROP_NO_RDP = 136, //PCROP can�t be enabled with RDP Level 0 � see errata sheet
};


       // ***************
       // Programmer type
       // ***************
enum PROG_TYPE { //ProgrammerType,  don't change
  PROG_ICP01               = 0,
  PROG_ICP2                = 1,
  PROG_GANG4               = 2,
  PROG_GANG4_SINGLECHAN    = 3,
  PROG_PORTABLE            = 4,
  PROG_LAST
};

enum GANG_TYPE {
  GANGTYPE_GANG4           = 0,
  GANGTYPE_COMBO           = 1,
};

       // ****************
       // Programming mode
       // ****************
enum PROG_MODE { //Mode, don't change
  SPM_PCDRIVEN    = 0,
  SPM_STANDALONE  = 1,
  SPM_LAST
};

       // **************************
       // Usage of user's RAM buffer
       // **************************
enum RAM_BUF_USAGE {
  RAM_BUF_NO       = 0, //not used (default)
  RAM_BUF_PM       = 1, //override PM
  RAM_BUF_DM       = 2, //override DM (EEPROM)
  RAM_BUF_PM_DM    = 3, //override PM and DM (128 per each, for future use)
  RAM_BUF_SEC_COM  = 4  //security feature related communication between PC and ICP
};


       // ******************
       // Environment Status
       // ******************
enum PRJ_VAL { //don't change
  prjUNKNOWN = 0, //unknown state (for PC only)

  prjLOADING = 1, //project loading/validation in progress
  prjINVALID = 2, //invalid
  prjVALID   = 3, //valid

};

        // *****************************************************
        // Fuse types, also can be used for special memory areas
        // (see c_icpexp.h)
        // *****************************************************
enum FUSE_TYPES {
  FU_TYPE_NO         = 0, //(no special description)
  FU_TYPE_LOW        = 1, //Fuse Low Byte
  FU_TYPE_HIGH       = 2, //Fuse High Byte
  FU_TYPE_EXTENDED   = 3, //Fuse Extended Byte
  FU_TYPE_LOCK       = 4, //Lock Bits Byte
  FU_TYPE_CALIB      = 5, //Calibration Byte
  FU_TYPE_DEV_ID     = 6, //Device ID (signature)

  FU_TYPE_LOCK_31_0   = 7,  //Lock Bits 31-0
  FU_TYPE_LOCK_63_32  = 8,  //Lock Bits 63-32
  FU_TYPE_LOCK_95_64  = 9,  //Lock Bits 95-64
  FU_TYPE_LOCK_127_96 = 10, //Lock Bits 127-96

  FU_TYPE_USER_SIGN   = 11, //User signature
  FU_TYPE_UNIQUE_ID   = 12, //Unique Identifier (unique S/N)

  FU_TYPE_USER_ROW    = 13, //User row
  FU_TYPE_FAMILY_ID   = 14, //Family ID
  FU_TYPE_SKIP        = 15, //Skip all operations with this fuse (reserved fuse)

  FU_TYPE_PROD_SIGN   = 16, //Production signature
  FU_TYPE_BOOT_VERS   = 17, //Boot code version

  //add new types here
  FU_ATMEL_DUMMY = 255
};


#endif
