﻿Public Class ICP_2020
    ' ==================================================================
    '     Standard Functions
    ' ==================================================================
    ' Starts application. Should be called once
    ' aFileCfg: ICP-01 configuration file to be loaded, usually "icp01.cfg"
    Declare Function IcpStartApplication Lib "IcpDll.dll" (ByVal aFileCfg As String) As Long

    ' ------------------------------------------------------------------
    ' Initializes RS-232 port (mandatory)
    ' aOverCfg:  0-gets communication parameters from *.cfg file
    '            1-overrides *.cfg file with aPort and aBaudRate
    ' aPort:     0-COM1, 1-COM2,..., 6-COM7, 7-COM8
    ' aBaudRate: 0 - 38,400bps (programmers before June-2002)
    '            1 - 115,200bps
    Declare Function IcpInitCom Lib "IcpDll.dll" (ByVal aOverCfg As Long, ByVal aComPort As Long, ByVal aBaudRate As Long) As Long

    ' ------------------------------------------------------------------
    ' Loads hex (mandatory) and serialzation file (optional)
    Declare Function IcpLoadHexAndSerFile Lib "IcpDll.dll" (ByVal aFileHex As String, ByVal aFileSer As String) As Long

    ' ------------------------------------------------------------------
    ' Executes action according ACTION_LIST
    ' aAction: see enum ACTION_LIST
    ' aMemorySpace: sum of memory space values for a function (see enum MEMORY_SPACES),
    '               Example: set aMemorySpace=PM_SPACE+FU_SPACE to execute
    '               an operation on program memory and configuration word
    ' aPmUserRange: 0-use full PM range from database, 1-override
    ' aPmAddrBeg:   start address of program memory (if aPmUserRange==1)
    ' aPmAddrEnd:   end address of program memory (if aPmUserRange==1)
    ' aSaveResult:  1-operation result will be written to file "auto01.res"
    ' aReadFile:    hex file to be saved after read operation

    Declare Function IcpDoAction Lib "IcpDll.dll" (ByVal aAction As Long,
                  ByVal aMemorySpace As Long,
                  ByVal aPmUserRange As Long,
                  ByVal aPmAddrBeg As Long,
                  ByVal aPmAddrEnd As Long,
                  ByVal aSaveResult As Long,
                  ByVal aReadFile As String) As Long
    ' ------------------------------------------------------------------
    ' Releases RS-232 port
    Declare Function IcpReleaseCom Lib "IcpDll.dll" () As Long

    ' ------------------------------------------------------------------
    ' Terminates application (mandatory). Should be called once
    Declare Function IcpEndApplication Lib "IcpDll.dll" () As Long

    ' ------------------------------------------------------------------
    ' Reads string with DLL version
    Declare Function IcpReadDllVersion Lib "IcpDll.dll" (ByRef dllSoftwareVer As String) As Long


    ' ==================================================================
    '     Advanced Functions
    ' ==================================================================
    ' Gets actual PM range. May be useful for applications with simple GUI
    Declare Function IcpGetPmAddr Lib "IcpDll.dll" (ByRef PmStart As Long, ByRef PmEnd As Long) As Long

    ' ------------------------------------------------------------------
    ' Gets actual memory spaces selected for operations, see enum MEMORY_SPACES.
    ' May be useful for applications with simple GUI
    Declare Function IcpGetMemorySpace Lib "IcpDll.dll" (ByRef CheckBox As Long) As Long

    ' ------------------------------------------------------------------
    ' Gets and sets communication parameters, overrides settings from icp01.cfg
    ' May be useful for applications with simple GUI.
    ' Parameters as for function IcpInitCom(...)
    Declare Function IcpGetCommParm Lib "IcpDll.dll" (ByRef Port As Long, ByRef baudRate As Long) As Long
    Declare Function IcpSetCommParm Lib "IcpDll.dll" (ByVal Port As Long, ByVal baudRate As Long) As Long

    ' ------------------------------------------------------------------
    ' Sets PIC DM (EEPROM) range for operation
    ' aDmUserRange: 0-use full DM range from database, 1-override
    ' aDmAddrBeg:   start address of data memory (if aDmUserRange==1)
    ' aDmAddrEnd:   end address of data memory   (if aDmUserRange==1)
    Declare Function IcpSetDmRange Lib "IcpDll.dll" (ByVal aDmUserRange As Long,
                              ByVal aDmAddrBeg As Long,
                              ByVal aDmAddrEnd As Long) As Long

    ' ------------------------------------------------------------------
    ' Calculates unprotected checksum of the buffers according manufacturer's
    ' programming specifications.
    Declare Function IcpCalcChecksum Lib "IcpDll.dll" () As Long


    ' ------------------------------------------------------------------
    ' Reads result of standalone operation for one channel
    Declare Function IcpReadStaResOneCh Lib "IcpDll.dll" (ByVal aCh As Long) As Long
    ' aCh: channel number, range 0...63. NOTE: aCh is 0 for ICP2 (not GANG)
    ' Returns:
    ' -1 if channel is not selected
    ' -2 if channel number is out of range (>63)
    ' errorcode for selected channel according to AUTO_ERROR_LEVEL

    ' ------------------------------------------------------------------
    ' Enables/disables progress window (progress bar)
    Declare Sub IcpEnableProgressWindow Lib "IcpDll.dll" (ByVal aEnable As Long)

    ' ------------------------------------------------------------------
    ' Enables/disables sound by overriding "Sound" preferences
    Declare Sub IcpSetSound Lib "IcpDll.dll" (ByVal aSoundOn As Long)

    ' ------------------------------------------------------------------
    ' Get current value of "Sound" preferences (0-disabled; 1-enabled)
    Declare Function IcpGetSound Lib "IcpDll.dll" () As Long

    ' ------------------------------------------------------------------
    ' Enables/disables chip erasing before programming by overriding
    ' "Clear flash device before programming" preferences
    Declare Sub IcpSetChipEraseBeforeProg Lib "IcpDll.dll" (ByVal aEraseOn As Long)

    ' ------------------------------------------------------------------
    ' Turns ON/OFF the "sleep" setting for chip programming and reading
    ' operations. When this setting is ON, computer processor load is less,
    ' but the operations may execute slower.
    ' aTurnSleepOn: 0 - turn sleep off; 1 - turn sleep on
    Declare Sub IcpSetSleepOnWaitingRS232 Lib "IcpDll.dll" (ByVal aTurnSleepOn As Long)

    ' ------------------------------------------------------------------
    ' aEnbl = 0 : update serialization file only after successful programming
    ' aEnbl = 1 : always update serialization file
    Declare Sub IcpAlwaysUpdateSer Lib "IcpDll.dll" (ByVal aEnbl As Long)

    ' *********************************************
    ' Standalone operation
    ' 1) Save environment(project) to a file
    ' 2) Transfer saved environment file to programmer
    ' *********************************************
    Declare Function IcpSaveEnvironment Lib "IcpDll.dll" (ByVal aFileName As String) As Long
    ' Saves environment (all current settings, hex file, etc) to file "aFileName",
    ' use extension .pj2 for aFileName 
    ' Returns errorcode according to AUTO_ERROR_LEVEL

    Declare Function IcpTransferEnvironmentToIcp Lib "IcpDll.dll" (ByVal aFileName As String) As Long
    ' Sends environment file "aFileName" (*.pj2) to programmer
    ' Returns errorcode according to AUTO_ERROR_LEVEL



    ' *****************
    ' Select Programmer
    ' *****************
    Declare Function IcpDllSelectProg Lib "IcpDll.dll" (ByVal aProg As Long, ByVal aMode As Long, ByVal aCh As Long) As Long
    'Description: Selects programmer type and mode by overwriting value from *.cfg file
    'Parameters:
    '  aProg - programmer according to enum PROG_TYPE
    '  aMode - mode of operation according to enum PROG_MODE
    '  aCh - current channel for "GANG-single channel" programmer (value 0 for channel 1, 1 for
    ' channel 2, etc.)

    'Return value: A) AUTO_OK if parameters are correct
    '              B) -1 if parameter 1 is not correct
    '                 -2 if parameter 2 is not correct
    '                 -3 if parameter 3 is not correct



    ' **********************************************
    ' Read single word (calibration, Device ID, etc) 
    ' **********************************************
    Declare Function IcpSingleWordRead Lib "IcpDll.dll" (ByVal aLoadConfig As Long, ByVal aIncValue As Long, ByRef aWord As Long) As Long
    ' aLoadConfig: 1-access configuration memory, 0-access PM
    ' aIncValue:   address increment (in words) from beginning of the accessed area
    ' aWord:       read word

End Class
