// See "dll_desc.txt" file for details

#ifndef C_ICPEXP_H
#define C_ICPEXP_H

#pragma pack(1)

#ifdef __cplusplus
  extern "C" {
#endif

#include "Fr_exp.h"

# ifdef ICP_BUILD_DLL
#   define DLL_FUNC __declspec(dllexport) __stdcall
# else
#   define DLL_FUNC __declspec(dllimport) __stdcall
# endif

typedef const char* ICPSTR;

// ==================================================================
//     Standard Functions
// ==================================================================

       // *****************
       // Start apllication
       // *****************
int DLL_FUNC  IcpStartApplication (char *aFileCfg);
// Starts ICP application. Should be called ***once***
// aFileCfg: ICP-01 configuration file to be loaded, usually "icp01.cfg"

       // ********
       // Open COM
       // ********
int DLL_FUNC  IcpInitCom (int aOverCfg, int aComPort, int aBaudRate);

// Initializes COM port (mandatory)
// aOverCfg:  0-gets communication parameters from *.cfg file
//            1-overrides *.cfg file with aPort and aBaudRate
// aPort:     0-COM1, 1-COM2,...
// aBaudRate: see above: enum BAUD_RATES
//            0 - 38,400bps (not used)
//            1 - 115,200bps (ICP2 with RS-232 connection)
//            2 - 230,400bps (not used)
//            3 - 460,800bps (ICP2 with USB or LAN connection)
//            4 - 921,600bps (G3 with USB connection)
//            NOTE: ICP software will automatically find the best baud rate
//                  during operation

       // ********************************
       // Load HEX and serialization files
       // ********************************
int DLL_FUNC  IcpLoadHexAndSerFile (char *aFileHex, char *aFileSer);
// Loads hex (required for PC-driven mode) and serialzation file
//(optional for both PC-driven and Standalone modes)


       //*****************
       //Load Security Bit
       //*****************
//Loads security bit to programming "buffer" for devices with unmapped security
//bit (i.e. if it can't be got from HEX file). Example: ATSAMC21 family
//1=Enable, 0=Disable (default)
//Always returns 0
//This function does nothing for devices with mapped security/protection bit(s)
//IMPORTANT: this operation doesn't execute programming itself, IcpDoAct with
//ACT_PROG parameter should be called
int DLL_FUNC IcpLoadSecurityBit(int aOn);


       // ***************************************
       // Execute programming, verification, etc.
       // ***************************************
#define SAVE_SHORT_RES      1
#define SAVE_EXTENDED_RES   2
int DLL_FUNC  IcpDoAction( int aAction,
                  unsigned int aMemorySpace,
                  unsigned int aPmUserRange,
                  unsigned int aPmAddrBeg,
                  unsigned int aPmAddrEnd,
                  unsigned int aSaveResult,
                  char* aReadFile );

// Executes action according ACTION_LIST
// aAction: see enum ACTION_LIST
// NOTE: all parameters below are not valid for Standalone operations
// aMemorySpace: sum of memory space values for a function (see enum MEMORY_SPACES),
//               Example: set aMemorySpace=PM_SPACE+FU_SPACE to execute
//               an operation on program memory and configuration word
// aPmUserRange: 0-use full PM range from database, 1-override
// aPmAddrBeg:   start address of program memory (if aPmUserRange==1)
// aPmAddrEnd:   end address of program memory (if aPmUserRange==1)
// aSaveResult:  1-operation result will be written to file "auto01.res"
// aReadFile:    hex file to be saved after read operation


       // ****************
       // Release COM port
       // ****************
int DLL_FUNC  IcpReleaseCom (void);
// Releases COM port (RS-232/USB/LAN)

       // *************************
       // Terminate ICP application
       // *************************
int DLL_FUNC  IcpEndApplication (void);
// Terminates application (mandatory). Should be called once

       // ****************
       // Read DLL version
       // ****************
int DLL_FUNC  IcpReadDllVersion (char *dllSoftwareVer);
// Reads string with DLL version

       // ***************************************************
       // Read result of standalone operation for one channel
       // ***************************************************
int  DLL_FUNC IcpReadStaResOneCh(unsigned int aCh);
// aCh: channel number, range 0...63. NOTE: aCh is 0 for ICP2 (not GANG)
// Returns:
// -1 if channel is not selected
// -2 if channel number is out of range (>63)
// errorcode for selected channel according to AUTO_ERROR_LEVEL

       // *********************************************
       // Enable/disable progress window (progress bar)
       // *********************************************
void     DLL_FUNC  IcpEnableProgressWindow(int aEnable);
// Enables/disables progress window (progress bar)

// ==================================================================
//     Advanced Functions
// ==================================================================

       // *********************************************
       // Standalone operation
       // 1) Save environment(project) to a file
       // 2) Transfer saved environment file to programmer
       // *********************************************
int  DLL_FUNC IcpSaveEnvironment (const char* aFileName);
// Saves environment (all current settings, hex file, etc) to file "aFileName",
// use extension .pj2 for aFileName
// Returns errorcode according to AUTO_ERROR_LEVEL

int  DLL_FUNC IcpTransferEnvironmentToIcp (const char* aFileName);
// Sends environment file "aFileName" (*.pj2) to programmer
// Returns errorcode according to AUTO_ERROR_LEVEL

int  DLL_FUNC IcpValidateEnvironmentFile (const char* aFileName);
// Validate environment file "aFileName" (*.pj2)
// Returns errorcode according to AUTO_ERROR_LEVEL


       // **********************
       // "Sleep" setting for PC
       // **********************
void DLL_FUNC IcpSetSleepOnWaitingRS232(int aTurnSleepOn);
// Turns ON/OFF the "sleep" setting for chip programming and reading
// operations. When this setting is ON, computer processor load is less,
// but the operations may execute slower.
// aTurnSleepOn: 0 - turn sleep off; 1 - turn sleep on

       // *******************
       // Get actual PM range
       // *******************
int DLL_FUNC  IcpGetPmAddr(unsigned int *PmStart, unsigned int *PmEnd);
// Gets actual PM range. May be useful for applications with simple GUI

       // *******************
       // Get actual DM range
       // *******************
int DLL_FUNC  IcpGetDmAddr(unsigned int *DmStart, unsigned int *DmEnd);
// Gets actual DM range. May be useful for applications with simple GUI

       // ***********************
       // Get actual memory space
       // ***********************
int DLL_FUNC  IcpGetMemorySpace(unsigned short *CheckBox);
// Gets actual memory spaces selected for operations, see enum MEMORY_SPACES.
// May be useful for applications with simple GUI

       // ***********************
       // Set actual memory space
       // ***********************
void DLL_FUNC IcpSetMemorySpace(unsigned short CheckBox);

       // ************************************
       // Get and set communication parameters
       // ************************************
int DLL_FUNC  IcpGetCommParm(int *Port, int *baudRate);
int DLL_FUNC  IcpSetCommParm(int Port, int baudRate);
// Gets and sets communication parameters, overrides settings from *.cfg
// May be useful for applications with simple GUI.
// Parameters as for function IcpInitCom(...)

       // *********************
       // Set DM (EEPROM) range
       // *********************
int  DLL_FUNC IcpSetDmRange ( int aDmUserRange,
                              unsigned int aDmAddrBeg,
                              unsigned int aDmAddrEnd);
// Sets PIC DM (EEPROM) range for operation
// aDmUserRange: 0-use full DM range from database, 1-override
// aDmAddrBeg:   start address of data memory (if aDmUserRange==1)
// aDmAddrEnd:   end address of data memory   (if aDmUserRange==1)

       // ************
       // Calculate CS
       // ************
unsigned DLL_FUNC  IcpCalcChecksum(void);
// Calculates unprotected checksum (ICP2 Legacy Checksum) of the buffers according manufacturer's
// programming specifications.


       // ******************************************************
       // Enable/disable sound by overriding "Sound" preferences
       // ******************************************************
void DLL_FUNC IcpSetSound(int aSoundOn);
// Enables/disables sound by overriding "Sound" preferences

       // ***************************************
       // Get current value of "Sound" preference
       // ***************************************
int  DLL_FUNC IcpGetSound(void);
// Get current value of "Sound" preferences (0-disabled; 1-enabled)

       // *************************
       // Enable/disable bulk erase
       // *************************
void DLL_FUNC IcpSetChipEraseBeforeProg(int aEraseOn);
// Enables/disables chip erase before programming by overriding
// "Clear flash device before programming" preferences
int DLL_FUNC IcpGetChipEraseBeforeProg(void);
// Get current value of this preference setting


       // ************************
       // Enable/disable row erase
       // ************************
void DLL_FUNC IcpSetRowEraseBeforeProg(int aEraseOn);
// Enables/disables row erase before programming by overriding
// "Row erase device before programming" preferences
// NOTES:
// 1) When on:
//    - only selected PM range is erased for PIC10/12/16
//    - entire chip is erased for dsPIC30 family
// 2) Row erase is supported for limited PICs - contact Softlog Systems
//    for details


void DLL_FUNC IcpSetHardEraseBeforeProg(int aEraseOn);
// Enables/disables hardware erase before programming by overriding
// "Smart hardware erase" (T_DIO_2) preference
// Hardware erase is supported for limited devices - contact Softlog Systems
// for details


       // ********************************
       // Enable/disable Enhanced ICSP(TM)
       // ********************************
int DLL_FUNC IcpSetEnhancedProg(int aEnhancedOn);
// Enables/disables Enhanced ICSP(TM)
// aEnhancedOn=1: enable Enhanced ICSP(TM)
// aEnhancedOn=0: enable low-level programming
// Returns:
// 0:  OK
// -1: Enhanced ICSP(TM) is not supported (8-bit devices, some 16-bit devices)
// -2: Low-level ICSP(TM) is not supported (PIC32 family)


       // ********************************************************
       // Writes or reads a byte to/from ICP's non-volatile memory
       // ********************************************************
#define USER_BUF_SIZE_24LC01  64  //64 bytes of 24LC01 are available
int DLL_FUNC  IcpSaveByte24Lc01(unsigned char aOffset, unsigned char aData);
int DLL_FUNC  IcpReadByte24Lc01(unsigned char aOffset, unsigned char *aData);
// Writes(saves) or reads a byte to/from ICP's internal non-volatile memory.
// May be used for secure applications
// NOTE: aOffset range: 0...(USER_BUF_SIZE_24LC01-1), i.e. 0...63

       // *********************************
       // Full access to ICP memory buffers
       // *********************************
// Provides full access to all ICP-1 buffers (PM, ID, DM, CM and FU)
// Be careful when modifying programming buffers.
// Suitable for applications that require buffer manipulation
// without (or in addition to) using hex file.
typedef struct { //original structure, not suitable for dsPIC
    int      PmWordSize;     // Program Memory word size
    unsigned PmMaxWordVal;   // Program Memory maximal word value
    int      PmAddrUnit;     // Program Memory address unit:
                             //   size (in bytes) of addressed memory chunk.
                             //   Deprecated: do not use in new projects.
    char    *PmBuf;  // Program Memory
    int      PmSize;
    char    *IdBuf;  // ID Memory
    int      IdSize;
    char    *DmBuf;  // Data Memory
    int      DmSize;
    char    *CmBuf;  // Calibration Memory
    int      CmSize;
    char    *FuBuf;  // Fuses Memory
    int      FuSize;
} iBUFFERS;


// iBUFFERS_EX: extension of iBUFFERS struct
typedef struct { //extended structure
    int      PmAddrStep;   // PM: difference between two valid successive addresses
    int      DmAddrStep;   // DM: ditto
    int      DmWordSize;   // Data Memory word size
    unsigned DmMaxWordVal; // Data Memory maximal word value
    int      DmAddrStart;  // Data Memory start address
    char    *BmBuf;        // Boot Memory
    int      BmSize;
    int      Reserved[29]; // For future use
} iBUFFERS_EX;

void     DLL_FUNC  IcpGetBuffers   (iBUFFERS *aBufs);
void     DLL_FUNC  IcpGetBuffersEx (iBUFFERS_EX *aBufs);

typedef struct {
    const char* HexFName;
    const char* SqtpFName;
    int      SqtpEnabled;
    char     SqtpString[20];
} iDISPLAY;
void     DLL_FUNC  IcpGetDisplayValues  (iDISPLAY *aDisp);

       // ************************************************
       // Full access to ICP memory buffers (byte-by-byte)
       // ************************************************
int DLL_FUNC IcpBufWr (int aBufType, unsigned int aOffset, unsigned char aData);
//Writes a byte to a buffer
//aBufType: buffer type according to enum MEMORY_SPACES (i.e.PM_SPACE, ID_SPACE, etc)
//aAddr: offset in buffer in ***bytes***
//aData: value to be written
// Returns:
//  0: OK
// -1: incorrect aBufType
// -2: incorrect aOffset

int DLL_FUNC IcpBufRd (int aBufType, unsigned int aOffset, unsigned char* aData);
//Reads a byte from a buffer
//aBufType: buffer type according to enum MEMORY_SPACES
//aAddr: offset in buffer in ***bytes***
//aData: pointer to a read byte
// Returns:
//  0: OK
// -1: incorrect aBufType
// -2: incorrect aOffset


       // ***************************
       // Update during serialization
       // ***************************
void     DLL_FUNC  IcpAlwaysUpdateSer(int aEnbl);
//aEnbl=0 : update serialization file after successful programming only
//aEnbl=1 : update serialization file after any programming

       // *********************
       // Generate key (Keeloq)
       // *********************
int DLL_FUNC IcpGenKey(unsigned char* aKey, unsigned char* aSeed);
//aKey:  8 byte result
//aSeed: 8 byte array (S/N - serial number)

/*
// Example:
int GenerateKey(void)
{
static unsigned char Key[8];
unsigned char Seed[8] = {0x00, 0x10, 0,0,0,0,0,0}; // S/N = 0x1000
int err;

  err=IcpGenKey(Key,Seed);
  if (! err) //successful key generation
  {
    //Key[0]...Key[7] contain a result
    //if manufacturer key = "0123456789abcdef" and S/N=0x1000 then
    //Key[]=0x7f,0x6b,0x4c,0xbc,0x94,0x00,0xed,0x5e
  }
  else //error
  {
    //see error list AUTO_ERROR_LEVEL
  }
}
*/

       // ****************************
       // Calibration memory operation
       // ****************************
// Erase chip or special area - contact Softlog for details
// ********************************************************
int DLL_FUNC IcpEraseCalib(int aLoadConfig, int aIncValue, int aEraseSeq);
// aLoadConfig: 1-access configuration memory, 0-access PM
// aIncValue:   address increment (in words) from beginning of the accessed area
// aEraseSeq:   number of erase sequence (in ICP2 family firmware)
// Example: erase User ID by row erase for PIC16F1xxx families:
//          IcpEraseCalib (1, 0, 133); //firmware 18.11 or higher


// Read single (calibration) word. Also reads Device ID (PIC12F/16F, PIC18F,
// PIC24/dsPIC33, ATmega8, etc.) or other special memories
// *************************************************************************
int  DLL_FUNC IcpSingleWordRead(int aLoadConfig, int aAddrOrIncValue,  unsigned short *aWord);
// aLoadConfig: 1-access configuration memory, 0-access PM
// aIncValue:   address or increment (in words) from beginning of the accessed area
// aWord:       read word

/* NOTE: also reads Device ID as follows:
   unsigned short dev_id;
   IcpSingleWordRead(1, 6, &dev_id); //for all PIC12F/16F families
   IcpSingleWordRead(0, 0x3FFFFE, &dev_id); //for all PIC18F families
   IcpSingleWordRead(0, 0xFF0000, &dev_id); //PIC24/dsPIC33: DEVID
   IcpSingleWordRead(0, 0xFF0002, &dev_id); //PIC24/dsPIC33: DEVREV
*/
/*
   Atmel ATmega8: read additional areas
   - aLoadConfig: fuse type according to enum FUSE_TYPES
   - aIncValue: offset of byte (address  bb in protocol)

   IcpSingleWordRead(FU_TYPE_DEV_ID, 0, &dev_id); //manufacturer (Atmel)
   IcpSingleWordRead(FU_TYPE_DEV_ID, 1, &dev_id); //flash size
   IcpSingleWordRead(FU_TYPE_DEV_ID, 2, &dev_id); //family

   IcpSingleWordRead(FU_TYPE_CALIB,  0, &dev_id); //calibration location 0
   ...
   IcpSingleWordRead(FU_TYPE_CALIB,  3, &dev_id); //calibration location 3
*/

/*
   Atmel SAM E/S/V (Cortex M7): read additional areas
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
     FU_TYPE_DEV_ID: Chip Identifier (4 bytes of CHIPID_CIDR and 4 bytes of CHIPID_EXID)
     FU_TYPE_CALIB: Calibration Bits
     FU_TYPE_USER_SIGN: User Signature Area (512 bytes)
     FU_TYPE_UNIQUE_ID: Unique Identifier Area (16 bytes)
   - aIncValue: offset (in ***byte*** units)
   - aWord: result (only bits <7:0> are valid)

*/

/*
   Atmel UPDI Tiny: read additional areas
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
     FU_TYPE_USER_ROW: User Row
     FU_TYPE_FAMILY_ID: Family ID
     FU_TYPE_NO: other memories (according to address, for example 0x1100=start of SIGROW)
   - aIncValue: absolute address (in ***byte*** units)
   - aWord: result (only bits <7:0> are valid)

*/

/*
   Atmel TPI Tiny (ATtiny20/40/102/104/4/5/9/10): read additional areas
   - aLoadConfig: dummy (use parameter 0)
   - aIncValue: absolute address (in ***byte*** units, as in datasheet)
   - aWord: result (only bits <7:0> are valid)
*/

/*
   Atmel ATXMEGA: read additional areas
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
     FU_TYPE_DEV_ID: Device ID and revision
     FU_TYPE_USER_SIGN: User signature row
     FU_TYPE_PROD_SIGN: Production signature row
   - aIncValue: offset (in ***byte*** units)
   - aWord: result (only bits <7:0> are valid)
*/

/*
   LPC80x: read additional areas
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
     FU_TYPE_DEV_ID: Device ID and revision (4 bytes)
     FU_TYPE_BOOT_VERS: Boot code version (2 bytes)
     FU_TYPE_UNIQUE_ID: Unique Identifier Area (16 bytes)
   - aIncValue: offset (in ***byte*** units)
   - aWord: result (only bits <7:0> are valid)
*/

/*
   STM32F2xx/4xx/7xx, STM32L0xx/1xx: read additional areas
   - aLoadConfig: dummy (use parameter 0)
   - aIncValue: absolute address (in ***byte*** units, as in datasheet)
   - aWord: result (only bits <7:0> are valid)
*/




// Write single (calibration) word
// *******************************
int  DLL_FUNC IcpSingleWordWrite(int aLoadConfig, int aIncValue,  unsigned short aWord);
// aLoadConfig: 1-access configuration memory, 0-access PM
// aIncValue:   address increment (in words) from beginning of the accessed area
// aWord:       word to be written

/*
   Atmel SAM E/S/V (Cortex M7): write User Signature Area
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
   - aIncValue: offset (in ***byte*** units)
   - aWord: value (***byte***) to be written

   IcpSingleWordWrite(FU_TYPE_USER_SIGN, <offset>,  <value>);
*/

/*
   Atmel UPDI Tiny: write additional area (User Row)
   - aLoadConfig: FU_TYPE_USER_ROW ("fuse" type according to enum FUSE_TYPES)
   - aIncValue: absolute address (in ***byte*** units), range 0x1300-0x131F
   - aWord: value (***byte***) to be written
*/

/*
   Atmel ATXMEGA: write additional area
   - aLoadConfig: "fuse" type according to enum FUSE_TYPES
     FU_TYPE_USER_SIGN: User signature row
   - aIncValue: offset (in ***byte*** units, maxumum according to 1 page)
   - aWord: value (***byte***) to be written
*/


/*
// Example for PIC16F684
// Description: PIC16F684 contains 1 calibration word located in configuration
// memory @0x2008

#define LOAD_CONFIG     1 //configuration memory should be accessed
#define INC_2008        8 //8 increments from beginning of configuration memory
#define ERASE_SEQ_3     3 //erase sequence 3 in ICP firmware

int ReadAndRestoreCalib2008(void)
{
int err=0;
static unsigned short calib_old, calib_new;


  for (;;)
  {
    // Read calibration word before programming
    // ----------------------------------------
    err=IcpSingleWordRead(LOAD_CONFIG, INC_2008, &calib_old);
    if (err) //error
      break;

    // Analyze read value
    // ------------------
    // ... Check (validate) calib_old if your expect a specific result

    // Execute usual programming (simple example, your actions may be more complcated)
    // ------------------------------------------------------------------------------
    err=IcpDoAction(ACT_PROG,ALL_SPACE,0,0,0,0,"Dummy file");
    if (err)
      break;

    // Read calibration after programming
    // ----------------------------------
    err=IcpSingleWordRead(LOAD_CONFIG, INC_2008, &calib_new);
    if (err)
      break;

    // Compare calibration word
    // ------------------------
    if (calib_new == calib_old) //programming OK
      break;

    // Calibration word corrupted
    // ==========================

    // Erase ***all*** chip ***including*** calibration word @0x2008
    // -------------------------------------------------------------
    err=IcpEraseCalib(LOAD_CONFIG, INC_2008, ERASE_SEQ_3);
    if (err)
      break;

    // Program chip again
    // ------------------
    err=IcpDoAction(ACT_PROG,ALL_SPACE,0,0,0,0,"Dummy file");
    if (err)
      break;

    // Restore original (old) calibration word
    // ---------------------------------------
    err=IcpSingleWordWrite(LOAD_CONFIG, INC_2008, calib_old);
    if (err)
      break;

    // Read calibration after programming
    // ----------------------------------
    err=IcpSingleWordRead(LOAD_CONFIG, INC_2008, &calib_new);
    if (err)
      break;

    // Compare calibration word
    // ------------------------
    if (calib_new != calib_old) //verification error
    {
      err=-1; //this is theoretical case only
      break;
    }

    // Programming successfully complete
    // ---------------------------------
    break;
  }

  return err;
}

*/

ICPSTR DLL_FUNC IcpGetErrorMessage(int aErrCode);



// =======================================================================
// =======================================================================
       // ******************************
       // Write from array to RAM buffer
       // ******************************
int DLL_FUNC IcpRamBufWrite(unsigned char *aRamBuf, int aStartAddr, int aQuan);
//Description: Communicates with ICP2 and writes 1 to 256 bytes to
//ICP2 RAM buffer + verification of written values
//Parameters:
//  aRamBuf - pointer to user's RAM buffer to be copied from
//  NOTE: single byte operation is not supported due to offset aStartAddr in memcpy
//  aStartAddr-start address of the RAM buffer
//  aQuan - number of bytes to be written
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // *****************************
       // Read to array from RAM buffer
       // *****************************
int DLL_FUNC IcpRamBufRead(unsigned char *aRamBuf, int aStartAddr, int aQuan);
//Description: Communicates with ICP2 and reads 1 to 256 bytes from
//ICP2 RAM buffer
//Parameters:
//  aRamBuf - pointer to user's RAM buffer to be copied from
//  NOTE: single byte operation is not supported due to offset aStartAddr in memcpy
//  aStartAddr-start address of the RAM buffer
//  aQuan - number of bytes to be read
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // ***************************
       // Set usage of the RAM buffer
       // ***************************
int DLL_FUNC IcpRamUsage( int aPmStartAddr,
                           int aPmQuan,
                           int aDmStartAddr,
                           int aDmQuan,
                           int aUsage );

//Description: Communicates with ICP2 and defines usage of the RAM buffer
//Parameters:
//  aPmStartAddr - start address of PM to be overwritten by the RAM buffer
//  aPmQuan - length of overwritten block (PM)
//  aDmStartAddr - start address of DM to be overwritten by the RAM buffer
//  aDmQuan - length of overwritten block (DM)
//  aUsage - usage of RAM buffer according to enum RAM_BUF_USAGE
//  NOTE: start address of PM/DM will be always overwritten by the beginning of
//        the RAM buffer (offset 0)
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // *********************************
       // Write a single byte to RAM buffer
       // *********************************
int DLL_FUNC IcpRamByteWr(unsigned char aData, int aAddr);
//Description: Communicates with ICP2 and writes 1 byte to
//ICP2 RAM buffer + verification of written values
//Parameters:
//  aData - data byte to be written
//  aAddr - address of the RAM buffer
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // **********************************
       // Read a single byte from RAM buffer
       // **********************************
int DLL_FUNC IcpRamByteRd(unsigned char *aData, int aAddr);
//Description: Communicates with ICP2 and reads 1 byte from ICP2 RAM buffer
//Parameters:
//  aData - pointer to user's data byte
//  aAddr - address of the RAM buffer
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct


       // ******************************************************************
       // Function A: Write a single byte to PC temporary buffer
       // Function B: Transfer the PC temporary buffer to the ICP RAM buffer
       // ******************************************************************
int DLL_FUNC IcpPcTmpBufWr(unsigned char aData, int aAddr);
//Description: Writes a byte to PC temporary buffer.
//NOTE: It does ***NOT*** communicate with ICP2
//Parameters:
//  aData - data byte to be written
//  aAddr - address of the buffer
//Return value:
//  0 if OK
// -1 if parameters are not correct

int DLL_FUNC IcpTrnasferPcTmpBufToRamBuf(int aStartAddr, int aQuan);
//Description: Communicates with ICP2 and transfers 1 to 256 bytes to
//ICP2 RAM buffer from the PC temorary buffer + verification of written values
//Parameters:
//  aStartAddr-start address of the RAM and PC temporary buffers
//  aQuan - number of bytes to be written
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct
// =======================================================================
// =======================================================================

       // *****************
       // Select Programmer
       // *****************
int DLL_FUNC IcpDllSelectProg(int aProg, int aMode, int aCh);
//Description: Selects programmer type and mode by overwriting value from *.cfg file
//Parameters:
//  aProg - programmer according to enum PROG_TYPE
//  aMode - mode of operation according to enum PROG_MODE
//  aCh - current channel for "GANG-single channel" programmer
//Return value: A) AUTO_OK if parameters are correct
//              B) -1 if parameter 1 is not correct
//                 -2 if parameter 2 is not correct
//                 -3 if parameter 3 is not correct


       // *****************************
       // Enable/disable COM re-opening
       // *****************************
void DLL_FUNC  IcpSetPortReopen (int aState);
//  aSet & 0x01: disable COM re-opening
//  aSet = 0:    enable COM re-opening

//  IcpStartApplication() calls IcpSetPortReopen(0x03), i.e. DLL default
//  state is:
//  - COM re-opening = disabled

// ===========================================================================
// Security Feature (***Some functions are empty***)
// ===========================================================================

       // *******************************
       // Structure of security data file
       // *******************************
#define SEC_STAMP      "SEC_ID_F"
#define SEC_ID_VER_L   0
#define SEC_ID_VER_H   1
#define SEC_ID_VERS    1
#define SIGN_SIZE      8
#define SEC_ID_SIZE    (16+1)
#define PASSW_SIZE     (16+1)
#define SEC_BUF_SIZE   64

enum SEC_BUF_DEST { //destination of security buffer
  SEC_BUF_FLASH   = 0,
  SEC_BUF_EEPROM  = 1

};


enum SEC_ID_UPD_MASK {
//IMPORTANT: some masks will be automatically overritten by DLL/firmware
//to keep security

  maskSEC_ID         = 0x0001, //update security ID
  maskPASSW          = 0x0002, //update password
  maskBATCH          = 0x0004, //update batch
  maskCNT            = 0x0008, //update counter
  maskSEC_BUF        = 0x0010, //update security buffer + its properties
                               //(length, destination, start addr, script)
  maskENV            = 0x0020, //update environment
  maskDEL_ENV_CNT    = 0x0040, //delete environment and counter

  maskDUMMY          = 0x8000, //dummy

};

typedef struct
{
  unsigned long Cs;  //checksum of the structure, sum of all bytes not including itself
  char Stamp[SIGN_SIZE];        //signature, fill with SEC_STAMP
  int VersL;                    //version of this structure (L) - fill with SEC_ID_VER_L
  int VersH;                    //+                         (H) - fill with SEC_ID_VER_H
  char SecId[SEC_ID_SIZE];      //Security ID
  char Passw[PASSW_SIZE];       //Password
  int BatchNum;	                //Batch number
  int DevQuan;                  //Number of devices to be programmed
  unsigned char SecBuf[SEC_BUF_SIZE];	//secure buffer
  int SecBufLen;                //actual secure buffer length
  int SecBufDest;               //secure buffer destination according to enum SEC_BUF_DEST
  int SecBufStartAddr;          //secure buffer start address
  int Script;                   //script number
  int UpdMask;                  //mask for updates, see enum SEC_ID_UPD_MASK

} SEC_ID_DATA;



       // *******************
       // Get ICP information
       // *******************
enum INFO_ICP_OPTIONS {
  INFO_OPT_DLL   = 0x0001, //DLL
  INFO_OPT_DSPIC = 0x0002, //dsPIC
  INFO_OPT_KEE   = 0x0004, //keeloq
  INFO_OPT_SEC   = 0x0008, //security feature
  INFO_OPT_PIC32 = 0x0010, //reserved for PIC32

  INFO_OPT_ALL   = 0xFFFF
};

#define INFO_ID_SIZE    3  //3 bytes for firmware ID
#define INFO_GANG_QUAN  64 //number of GANG channels

typedef struct //info from one channel of ICP2/ICP2-GANG
{
  int iValid; //1-information in this structure is valid
  unsigned short iProgType;  //programmer type according to PROG_TYPE
  unsigned short iFirmVer;   //firmware version (HL)
  unsigned char iIcpId[INFO_ID_SIZE]; //firmware ID (ICP serial number)
  unsigned short iBootVer;   //bootloader version (HL)
  unsigned short iIcpOpt;    //ICP enabled options according to INFO_ICP_OPTIONS
  unsigned short iFirmDevDb; //firmware device database version (HL)
  unsigned short iFirmPrjDb; //firmware environment (project) database version  (HL)
  unsigned short iDllDevDb;  //DLL device database version (HL)
  unsigned short iDllPrjDb;  //DLL project database version (HL)

  //Added 4-Nov-08 Vers. 4.5.5
  int iSecMode;            //security mode (1-security environment now)
  int iEnvStat;            //environment status according to enum PRJ_VAL
  char iEnvHexFileName[SEC_ID_SIZE]; //HEX file name inside the environment
  unsigned short iEnvHexFileCs;      //HEX file checksum (ICP2 Legacy Checksum)
  char iSecIdName[SEC_ID_SIZE];      //Security ID name
  int iSecCntValue;                  //value of non-volatile security counter
  int iSecCntInteg;                  //security counter integrity (0-OK)

  //Added 18-Nov-17
  int iProgG3;             //1=G3, 0=non-G3 products

  //Added 28-Feb-18
  unsigned char iG3PcbType;    //G3 PCB type (Softlog internal):
                               //0=ICP3M, 1=ICP2_PG3
  unsigned char iG3ProductId;  //G3 product ID (Softlog internal):
                               //0=ICP3M, 1=ICP2PORT(G3), 2...-internal/future use

  //Total number of ***bytes*** below must be ***128*** for compatibility
  unsigned short iSqtpRemained;    //G3 only, 25-Mar-19: SQTP remained numbers (0xFFFF=unlimited)
  unsigned short iTemperatureSens; //G3 only, 18-Nov-19: uC temperature sensor (10-bit)
  unsigned char iReserved[124]; //reserved for future members

} ICP_INFO;

int DLL_FUNC IcpDllGetInfoSingle(ICP_INFO *aInfo);
//Description: Communicates and reads information from the currently
//             active ***single*** channel
//Parameters:
//aInfo - pointer to ICP_INFO structure
//Return value: see AUTO_ERROR_LEVEL
//ICP info is returned in aInfo - see ICP_INFO structure


       // ****************************************************
       // Function A: Get ICP info into temporary PC structure
       // Function B: Read the result one-by-one
       // ****************************************************
// The 2 function below are useful if your development environment does not
// support arrays, otherwise use IcpDllGetInfoSingle()

enum WHAT_TO_READ {
  whatVALID              = 0,  //validity of information in this structure (***1*** if valid)
                               // ***IMPORTANT***: must be executed and tested for
                               // validity of the the rest of the data
  whatPROG_TYPE          = 1,  //programmer type according to PROG_TYPE
  whatFIRM_VER           = 2,  //firmware version (HL)
  whatICP_ID             = 3,  //firmware ID (ICP serial number), size: INFO_ID_SIZE
  whatBOOT_VER           = 4,  //bootloader version (HL)
  whatICP_OPT            = 5,  //ICP enabled options according to INFO_ICP_OPTIONS
  whatFIRM_DEV_DB        = 6,  //firmware device database version (HL)
  whatFIRM_PRJ_DB        = 7,  //firmware environment (project) database version (HL)
  whatDLL_DEV_DB         = 8,  //DLL device database version (HL)
  whatDLL_PRJ_DB         = 9,  //DLL project database version (HL)

  whatSEC_MODE           = 10, //security mode of environment
  whatENV_STAT           = 11, //environment status according to enum PRJ_VAL
                               // ***IMPORTANT***: must be done before analyzing
                               // the data below (data below is valid if a result
                               // equal to ***prjVALID***)
  whatENV_HEX_FILE_NAME  = 12, //HEX file name inside the environment,
                               //size: SEC_ID_SIZE (17 bytes maximum)
  whatENV_HEX_FILE_CS    = 13, //HEX file checksum (ICP2 Legacy Checksum)
  whatSEC_ID_NAME        = 14, //Security ID name, size: SEC_ID_SIZE
  whatSEC_CNT_VALUE      = 15, //value of non-volatile security counter
  whatSEC_CNT_INTEG      = 16, //security counter integrity (0-OK)
  whatPROG_G3            = 17, //G3/non-G3 product (0=non-G3, 1=G3)
  whatG3_PCB_TYPE        = 18, //G3: PCB type (0=G3_PCB_ICP3M, 1=G3_PCB_ICP2PG3)
  whatG3_PRODUCT_ID      = 19, //G3: Product ID (0=G3_PROD_ICP3M, 1=ICP2PORT(G3))
  whatSQTP_REMAINED      = 20, //G3: Remained serialization numbers (standalone SQTP)
  whatTEMPERATURE_SENS   = 21, //G3: uC temperature (10-bit A/D result)
  whatLAST_DUMMY               //dummy, indicates end of the enum
};

int DLL_FUNC IcpDllGetIcpInfoToPcTmpStruct(void);
//Description: Communicatates and reads information from the currently
//             active ***single*** channel into a temporary PC structure ICP_INFO
//Parameters:   none
//Return value: see AUTO_ERROR_LEVEL

//This function should be followed by series of IcpDllReadInfoOneByOne() below


int DLL_FUNC IcpDllReadInfoOneByOne(int aWhat, int *aData, int aIndex);
//Description: Reads information integer-by-integer that was get by function
//IcpDllGetInfoPcTmpStruct()

//Parameters:
//aWhat   - what to read - see enum WHAT_TO_READ
//aData   - pointer to the read data
//aIndex  - index of data to be read in case of array (whatICP_ID,
//          whatENV_HEX_FILE_NAME, whatSEC_ID_NAME)

//Return value: 0:  parameters are OK
//              -1: parameter error


        // ********************************************************
        // Enable/disable Error Screen after Standalone Programming
        // ********************************************************
void DLL_FUNC IcpErrScreenStaPrg (int aEnbl);
//Enables/disables error screen after failed standalone programming (when
//progress window is enabled)
//aEnbl=0 (default): disable
//aEnbl=1 : enable


        // **************************************
        // Get currently selected programmer type
        // **************************************
//Description: gets currently selected programmer type
//Parameters: none
//Return value: according to enum PROG_TYPE
int DLL_FUNC IcpGetProgrammerType (void);

        // **************************
        // Get Gang selected Channels
        // **************************
// IMPORTANT: number of boxes has higher priority, i.e. if aBoxNumber == 2, then
// the maximum enabled channel is 7 (range 0...7)

//Description: gets currently selected GANG boxes and channels
//Parameters:
//  *aBoxNumber - pointer to number of GANG boxes (1 to 15, 0 is not valid)
//  aChannels - pointer to array of selected channels. NOTE:
//              aChannels should be declared as "char aChannels[64]"
//              0-channel is not selected

void DLL_FUNC IcpGetGangBoxList (int *aBoxNumber, char aChannels[]);

       // ************************
       // Test connection with ICP (1 channel)
       // ************************
//Description: communicates with the programmer as follows:
//if ICP2 then with ICP2
//if GANG then with channel 2
//if GANG(single channel) then with the selected channel
//No LEDs are activated on the programmer (blind connection)

//Return value: according to AUTO_ERROR_LEVEL

int DLL_FUNC IcpTestConnection(void);

       // ***********************************
       // Send Security ID data to programmer
       // ***********************************
//Description: Communicates with ICP2 and sends security ID data to all channels
//Parameters:
//  aSecId - pointer to SEC_ID_DATA structure
//Return value: according to AUTO_ERROR_LEVEL
int DLL_FUNC IcpSecSendSecIdToPrg( SEC_ID_DATA *aSecId);

       // *************************
       // Create secure environment
       // *************************
//Description: creates secure environment file
//Parameters:
//  aSecId - pointer to SEC_ID_DATA structure
//  aEnvName - name of the input environment file (*.pj2)
//  aSenName - name of the output secure environment file (*.sen)
//Return value: according to AUTO_ERROR_LEVEL
int DLL_FUNC IcpSecCreateSecEnv ( SEC_ID_DATA *aSecId,
                                  const char *aEnvName,
                                  const char *aSenName);

       // ****************************************************
       // Transfer secure environment file to a single channel
       // ****************************************************
//Description: Transfer secure environment to a single channel
//Parameters:
//  aSenName - name of the secure environment file (*.sen)
//Return value: according to AUTO_ERROR_LEVEL
int DLL_FUNC IcpSecTransferSecEnv (const char *aSenName);


       // *********
       // Self-Test
       // *********
int DLL_FUNC IcpSelfTest( unsigned char aPar0,
                          unsigned char aPar1,
                          unsigned char aPar2,
                          unsigned char aPar3 );



// ==================================================================
//     FTB9: Standard Functions
// ==================================================================

       // *****************
       // Start apllication
       // *****************
int DLL_FUNC  FtbStartApplication (char *aFileCfg);
// Starts FTB9 application. Should be called ***once***
// aFileCfg: configuration file to be loaded, usually "ftb9.cfg"

       // ********
       // Open COM
       // ********
int DLL_FUNC  FtbInitCom (int aOverCfg, int aComPort, int aBaudRate);

// Initializes COM port (mandatory)
// aOverCfg:  0-gets communication parameters from *.cfg file
//            1-overrides *.cfg file with aPort and aBaudRate
// aPort:     0-COM1, 1-COM2,...
// aBaudRate: see above: enum BAUD_RATES
//            1 - 115,200bps (FTB9 RS-232 connection)
//            3 - 460,800bps (FTB9 with USB connection)
//            NOTE: FTB software will find the best baud rate during operation

       // ****************
       // Release COM port
       // ****************
int DLL_FUNC  FtbReleaseCom (void);
// Releases COM port (RS-232/USB)

       // *************************
       // Terminate FTB application
       // *************************
int DLL_FUNC  FtbEndApplication (void);
// Terminates application (mandatory). Should be called once

       // ****************
       // Read DLL version
       // ****************
int DLL_FUNC  FtbReadDllVersion (char *dllSoftwareVer);
// Reads string with DLL version


       // *******************
       // Write to RAM buffer
       // *******************
int DLL_FUNC FtbRamBufWrite(unsigned char *aRamBuf, int aStartAddr, int aQuan);
//Description: Communicates with FTB9 and writes 1 to 256 bytes to
//ICP2 RAM buffer + verification of written values
//Parameters:
//  aRamBuf - pointer to user's RAM buffer to be copied from
//  aStartAddr-start address of the RAM buffer
//  aQuan - number of bytes to be written
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // ********************
       // Read from RAM buffer
       // ********************
int DLL_FUNC FtbRamBufRead(unsigned char *aRamBuf, int aStartAddr, int aQuan);
//Description: Communicates with FTB9 and reads 1 to 256 bytes from
//FTB9 RAM buffer
//Parameters:
//  aRamBuf - pointer to user's RAM buffer to be copied from
//  aStartAddr-start address of the RAM buffer
//  aQuan - number of bytes to be read
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // ***************************
       // Set usage of the RAM buffer
       // ***************************
int DLL_FUNC FtbRamUsage( int aUsage );

//Description: Communicates with FTB9 and defines usage of the RAM buffer
//  aUsage - usage of RAM buffer according to enum RAM_BUF_USAGE
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // *************************
       // Test connection with FTB9
       // *************************
//Return value: according to AUTO_ERROR_LEVEL

int DLL_FUNC FtbTestConnection(void);


       // *********
       // Self-Test
       // *********
int DLL_FUNC FtbSelfTest( unsigned char aPar0,
                          unsigned char aPar1,
                          unsigned char aPar2,
                          unsigned char aPar3 );



//===========================================================================
//       FTB9: Hardware Functions
//===========================================================================
// Contact Softlog Systems for detailed description of FTB9 hardware functions

       // **********************************
	     // FTB9 DLL Functions: Destination uC
 	     // **********************************
int DLL_FUNC FtbUcSet (int auC);  //select uC according to UC_DESTIN, default: main uC
int DLL_FUNC FtbUcGet (int *auC); //get selected uC according to UC_DESTIN

       // **********************************************************
       // FTB9 DLL Functions: Enable/disable switchable power supply
       // **********************************************************
int DLL_FUNC FtbLm2738OnOff(int aEnbl);

       // **********************************************************
       // FTB9 DLL Functions: Set/clear/pulse hardware driven by 595
       // **********************************************************
int DLL_FUNC FtbAction595 ( int aAct,        //action according to GEN_ACT
                         int a595,        //number of 595 shift register
 		                     unsigned char aMask, //pin(s) mask
 		                     unsigned char aInversed, //1-pin inversed
 		                     unsigned short aDelUs);

//aDelUs: delay in [us] as follows: if aAct==ACT_PULSE_xxx, then aDelUs=pulse length
//                                  else aDelUs=delay after operation


       // ********************************************
       // FTB9 DLL Functions: Read shadow bytes of 595
       // ********************************************
int DLL_FUNC FtbReadShadow595 (unsigned char *aBuf);


       // ************************************************************************
       // FTB9 DLL Functions: Static Activation/Deactivation of Individual Outputs
       //                     + delay 5ms
       // ************************************************************************
int DLL_FUNC FtbVuutRelOnOff(int aOn);
int DLL_FUNC FtbIcpRelOnOff(int aOn);
int DLL_FUNC FtbPnph0TrOnOff(int aOn);
int DLL_FUNC FtbPnph1TrOnOff(int aOn);
int DLL_FUNC FtbSsrOnOff(int aOn);
int DLL_FUNC FtbNpn0TrOnOff(int aOn);
int DLL_FUNC FtbNpn1TrOnOff(int aOn);
int DLL_FUNC FtbNpn2TrOnOff(int aOn);
int DLL_FUNC FtbNpn3TrOnOff(int aOn);
int DLL_FUNC FtbPnp0TrOnOff(int aOn);
int DLL_FUNC FtbPnp1TrOnOff(int aOn);
int DLL_FUNC FtbPnp2TrOnOff(int aOn);
int DLL_FUNC FtbPnp3TrOnOff(int aOn);


int DLL_FUNC FtbSelLinSw(int aSource); //select UUT source (linear, switchable or none)
                                    //see enum UUT_SRC

       // ********************************************************
       // FTB9 DLL Functions: Connect resistor for current measure
       //                     and related input of ADG408
       // ********************************************************
// NOTE: it's user's responsibility to disconnect other current
//       resistors

int DLL_FUNC FtbConnectCurResAndAdg(int aCurRes);

      // ******************************************************
       // FTB9 DLL Functions: Active pulse on Individual Outputs
       // ******************************************************
int DLL_FUNC FtbUutDischargePulse(unsigned short aDelUs);

       // **************************************
       // FTB9 DLL Functions: Select MUX Channel
       // **************************************
int DLL_FUNC FtbSelAdMux ( int aAdInp, 	   //A/D input of uC, see enum AD_INP
                           int aMuxCh);    //MUX channel (valid if MUX A/D selected)

       // ************************************
       // FTB9 DLL Functions: Set VUUT Voltage
       // ************************************
int DLL_FUNC FtbSetVoltLinSw ( int aUutSource,		  //output source: see enum UUT_SRC
 			             unsigned short amV,	      //output voltage, mV (range UUT_VOLT_MIN...UUT_VOLT_MAX
 			             unsigned char aRampMv,	 	  //ramp (mV/cycle), 0 means immediately
 			             unsigned char aRampDelMs); //delay between cycles

       // ******************************************
       // FTB9 DLL Functions: Execute A/D Conversion
       // ******************************************
int DLL_FUNC FtbHardAdConv ( int aAdInp, 	   //A/D input of uC, see enum AD_INP
                          int aMuxCh,     //MUX channel (valid if MUX A/D selected)
 			                    int aConvType,  //conversion type, see enum CONV_TYPE
 			                    unsigned char aQuan,	//number of conversions
 			                    unsigned short aDelUs,	//delay between conversions in us
 			                    unsigned short *amVAverMin, //result: average or peak(minimum) in mV
			                    unsigned short *amVMax );  	 //result: peak(maximum) result in mV

       // ***********************************
       // FTB9 DLL Functions: Measure Current
       // ***********************************
int DLL_FUNC FtbCurMeas (	int aCurRes,	 //measure on resistor according to enum CUR_TYPE
			                  int aGain,     //gain according to enum GAIN_TYPE
			                  int aConvType, //conversion type according to enum CONV_TYPE
 			                  unsigned char aQuan, //number of conversions
	                      unsigned short aDelUs, //delay between conversions in us
                        int aShortAfterMeas,   //short measurement resistor after operation
	                      unsigned long* aNaAverMin, //average or peak(minimum) result in nA
	                      unsigned long* aNaMax );   //peak(max) result in nA

       // ***************************************
       // FTB9 DLL Functions: Set UART Parameters
       // ***************************************
int DLL_FUNC FtbUartParam	( int aBaudRate, //baud rate, see enum UUT_BAUD_RATE
 			                      unsigned short aTimeoutMs); //Rx timeout in ms

       // ***************************************
       // FTB9 DLL Functions: Execute UART Action
       // ***************************************
//NOTE: UART data is located in RAM buffer - see functions:
// FtmRamBufWrite()
// FtmRamBufRead()
// FtmRamUsage()

int DLL_FUNC FtbUartAction ( int aAction, //action: see UART_ACTION
                             int aTxQuan, //number of bytes to be sent
                             unsigned int *aRxQuan ); //number of received bytes


       // ****************************
       // FTB9 DLL Functions: Safe Off
       // ****************************
int DLL_FUNC FtbSafeOff (void);

       // ********************************************
       // FTB9 DLL Functions: Read State of BUSY lines
       // ********************************************
int DLL_FUNC FtbReadBusyLines (unsigned char* aState); //see enum BUSY_MASKS

       // **************************************************
       // FTB9 DLL Functions: Write (save) calibration value
       // **************************************************
int DLL_FUNC FtbCalibWr ( int aRecNum,                     //record number, see enum FTB9_CALIB_REC
 		          unsigned short aDenominator);    //denominator (16 bits)

       // *********************************************************
       // FTB9 DLL Functions: Read calibration value (verify/use for software compensation)
       // *********************************************************
int DLL_FUNC FtbCalibRd  ( int aRecNum,                    //record number
 	                   unsigned char* aReadData);      //pointer to returned data (4 bytes)

       // ***********************************************
       // FTB9 DLL Functions: Enable/disable firmware compensation (using calibration)
       // ***********************************************
int DLL_FUNC FtbCompensEnDis (int aEnable); //1-enable, 0-disable


       // *************************************
       // FTB9 DLL Functions: Set any parameter
       // *************************************
int DLL_FUNC FtbAnyParam	( int aWhichParam, //see enum FTB9_ANY_PAR
                                  unsigned long aValue); //value

//===========================================================================
//       ICP2: Hardware Functions
//===========================================================================
// Contact Softlog Systems for detailed description of ICP2 hardware functions


       // **********************************************
       // ICP2 Hardware Function: Execute A/D Conversion
       // **********************************************
int DLL_FUNC Icp2AdConv(  int aAdInp,    //ICP2 A/D inputs, see enum ICP2_AD_LIST
				                  int aConvType, //conversion type, see enum ICP_CONV_TYPE
                          unsigned char aQuan, //number of conversions
                          unsigned short aDelUs, //delay between conversions in us
                          unsigned short *aMvAverMin, //result: average or peak(minimum) in mV
                          unsigned short *aMvMax );   //result: peak(maximum) in mV
//Description:
//Executes A/D conversion for selected pin
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct


       // **************************************
       // ICP2 Hardware Function: Set D/A Levels (not pins)
       // **************************************
int DLL_FUNC Icp2DaVolt  (int aDac, //DAC according to ICP2_DAC_LIST
 			                    unsigned short aMv); //desired voltage in [mV]
//Description:
//Presets voltage for internal ICP2 D/A (output is not affected)
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct

       // ******************************************
       // ICP2 Hardware Function: Set/Read Pin State
       // ******************************************
int DLL_FUNC Icp2PinState (int aPin,    //pin according to ICP2_PIN_LIST
 			                     int aState,  //pin operation according to ICP_PIN_ACTION
                           int *aResult); //read result (0 or 1)
                          //NOTE: see documentation for available pin operations
//Description:
//Activates/deactivates/defines as input selected pin
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameters are not correct


       // ***************************************************************
       // ICP2 Hardware Function: Set CLOCK/DATA Communication Parameters
       // ***************************************************************
int DLL_FUNC Icp2ClockDataComPar (int aSpeed, //clock speed (see enum CL_DA_SPEED)
              unsigned char aDelAnswerMs); //delay between aByte2 and answer byte in ms
//Description: sets CLOCK/DATA communication parameters
//Return value: according to AUTO_ERROR_LEVEL

       // ***************************************************************
       // ICP2 Hardware Function: Execute single CLOCK/DATA Communication
       // ***************************************************************
int DLL_FUNC Icp2SingleClockDataAction  (unsigned char aByte0, //Tx byte 0
                                         unsigned char aByte1, //Tx byte 1
					 unsigned char aByte2, //Tx byte 2
					 unsigned char *aByteAnswer); //pointer to answer byte
//Description: Transmits 3 bytes and receives 1 byte via CLOCK/DATA(PGC/PGD) lines
//Return value: according to AUTO_ERROR_LEVEL

       // ***************************************************************
       // ICP2 Hardware Function: Measure frequency (period) using CLOCK/DATA Communication
       // ***************************************************************
int DLL_FUNC Icp2SingleClockDataFreq    (unsigned char aByte0, //Tx byte 0
                                         unsigned char aByte1, //Tx byte 1
					 unsigned char aByte2, //Tx byte 2
					 unsigned char *aByteAnswer, //pointer to answer byte
                                         unsigned int *aPeriod_us);  //pointer to measured period [us]
//Description:
//1) Sequence:
//   - transmits 3 bytes
//   - measures period (aPeriod_us) of the ***2-nd pulse*** on DATA line
//     NOTE: the slave must generate 3 periods
//   - receives 1 answer byte via CLOCK/DATA(PGC/PGD) lines
//2) Timeout for every measured pulse is 6.5ms, total timeout for all pulses is about
//   20ms
//3) Icp2ClockDataComPar() should be called before this function to set delay aByteAnswer
//   which defines delay between end of the measured (2-nd) pulse and 1st clock of the answer
//Return value: according to AUTO_ERROR_LEVEL


       // *****************************************************************
       // ICP2 Hardware Function: Execute multiple CLOCK/DATA Communication
       // *****************************************************************
int DLL_FUNC Icp2MultiClockDataAction (int aNumberOfCommands); //number of CLOCK/DATA commands
//Description: Executes multiple communications via CLOCK/DATA(PGC/PGD) lines
//aNumberOfCommands should be 1...RB6_RB7_MULTI_MAX
//Data for CLOCK/DATA command are in RAM buffer starting from RAM_BUF_TX_DATA_START
//Answer bytes are in RAM buffer starting from RAM_BUF_RX_DATA_START
//Return value: A) according to AUTO_ERROR_LEVEL
//              B) -1 if parameter is not correct

       // *************************************
       // ICP2 Hardware Function: Kill Firmware (keep bootloader only)
       // *************************************
int DLL_FUNC Icp2KillFirmware (void);
//Description: "kills" firmware and leaves bootloader only
//Return value: according to AUTO_ERROR_LEVEL


       // ************************************************
       // ICP2 Hardware Function: Disable Buttons and LEDs
       // ************************************************
int DLL_FUNC Icp2ButLedDsbl (void);
//Description: internal use - disables background button and LED procedure
//             in order to execute pin-control commands
//Return value: according to AUTO_ERROR_LEVEL

       // ***************************************
       // ICP2 Hardware Function: Start Self-Test
       // ***************************************
int DLL_FUNC Icp2SelfTest (void);
//Description: internal use - starts self-test routine
//Return value: according to AUTO_ERROR_LEVEL

       // *************************************
       // ICP2 Hardware Function: ICP2 Safe Off
       // *************************************
int DLL_FUNC Icp2SafeOff (void);
//Description: Turns all ICP2 pins to default state (all pins are de-energized)
//Return value: according to AUTO_ERROR_LEVEL

       // ****************************************************
       // ICP2 Hardware Function: TTP Debug - Generate Pattern
       // (Softlog internal function)
       // ****************************************************
int DLL_FUNC Icp2TtpPatternDebug  (int aPin,  //pin according to ICP2_PIN_LIST (limited)
                                   int aPattern); //pattern
//Reception to RAM buffer may be implemented for debugging




//===========================================================================
//       Multiple Environment Operation
//===========================================================================
       // *****************************************
       // Multiple Environments: Switch environment
       // *****************************************
int DLL_FUNC IcpSwitchEnv (int aEnv);
//Description: communicates with programmer and simultaneously
//switches environment for all GANG channels
//Parameter: aEnv should be 0 (environment 1) or 1 (env. 2)
//Return value:
//A) according to AUTO_ERROR_LEVEL
//B) -1: environment number is out of range
//C) -2: operation is not supported (ICP-01)


       // **************************************************************
       // Multiple Environments: Enable/disable environment switch by PC
       // **************************************************************
int DLL_FUNC IcpEnblDsblEnvSw (int aEnbl);
//Description: overrides CFG file setting to enable/disable environment
//             switch by PC during environment-related operations
//Parameter: 1=enable(normal mode) or 0=disable(debug) switch by PC
//Return value: 0 (AUTO_OK)


       // ************************************************************************
       // Multiple Environments: Read current environment number and start address
       // (useful for debug only)
       // ************************************************************************
int DLL_FUNC IcpReadEnvStartAddr (int *aEnv, unsigned int *aAddr);
//Description: communicates with programmer and reads number of currently active
//selected environment and its start address in EEPROM
//Operates on a ***single*** channel only
//Parameter:
//-*aEnv-pointer to current environment: 0=environment 1, 1=env. 2, etc.
//-*aAddr-pointer to the start address
//Return value: according to AUTO_ERROR_LEVEL

       // ******************************************
       // Multiple Environments: Write start address
       // (Contact Softlog, don't use)
       // ******************************************
int DLL_FUNC IcpCreateEnvStartAddr (int aEnv, int aAddr);


enum CALC_RANGE {
  CALC_FULLRANGE = 0,
  CALC_BESTRANGE = 1,
};

       // *******************************
       // Calculate full or best PM range
       // *******************************
//NOTE: use calculated values directly with function IcpDoAction() as parameters
//aPmAddrBeg and aPmAddrEnd (aPmUserRange should be set to 1)

int DLL_FUNC IcpCalcPmRange( unsigned int *aStartAddr,
                             unsigned int *aEndAddr,
                             int aWhichRange );
//Description: calculates best of full PM range
//Parameter:
//-*aStartAddr-pointer to start address
//-*aEndAddr-pointer to end address
//-aWhichRange: 0-get full range, 1-get best range
//Return value: A) -1 if parameter is not correct
//              B) 0 if OK


       // *******************************
       // Calculate full or best DM range
       // *******************************
//NOTE: use calculated values directly with function IcpSetDmRange as parameters
//aDmAddrBeg and aDmAddrEnd (aDmUserRange should be set to 1)

int DLL_FUNC IcpCalcDmRange( unsigned int *aStartAddr,
                             unsigned int *aEndAddr,
                             int aWhichRange );
//Description: calculates best of full DM (EEPROM) range
//Parameter:
//-*aStartAddr-pointer to start address
//-*aEndAddr-pointer to end address
//-aWhichRange: 0-get full range, 1-get best range
//Return value: A) -1 if parameter is not correct
//              B) 0 if OK


       // **********************************
       // Enable/disable Gap Elimination(TM)
       // **********************************
int DLL_FUNC IcpSetGapElimination(int aArea, int aEnable);
//Enables/disables Gap Elimination(TM) for PM(flash) or/and DM(EEPROM)
//Call before applying IcpDoAction()
//aArea: PM_SPACE, DM_SPACE or PM_SPACE+DM_SPACE
//aEnable=1: enable
//aEnable=0: disable
// 0:  OK
// -1: incorrect aArea is specified


       // ***********************************************************
       // Read SQTP numbers from PC buffers (PC-controlled SQTP only)
       // ***********************************************************
enum SQTP_TO_READ {
  whatSQTP_ON      = 0, //1=SQTP on, 0=OFF
  whatSQTP_TYPE    = 1, //0=Random,1=Pseudo-Random,2=Sequential,3=User File
  whatSQTP_ADDR    = 2, //start address
  whatSQTP_QUAN    = 3, //quantity of serial numbers
  whatSQTP_VAL     = 4, //value of serial number (one byte)
  whatSQTP_INC     = 5, //increment value (valid for sequential only)
  whatSQTP_ACCESS  = 6, //0=retlw, 1=raw data
  whatSQTP_SPACE   = 7  //0=PM
};

int DLL_FUNC IcpReadSqtpFromPcArray (  unsigned int aCh,
                                       int aWhat,
                                       unsigned int *aData,
                                       unsigned int aIndex );

//Description: Reads serialization info from ***PC*** (not from programmer)
//for PC-control serialization in standalone mode

//Parameters:
//aWhat   - what to read - see enum SQTP_TO_READ
//aCh     - programmer channel (0=channel 1, 1=channel 2, etc.)
//aData   - pointer to the read data
//aIndex  - index of data to be read in case of array (whatSQTP_VAL)

//Return value: 0:  parameters are OK
//              -1: parameter error
//              -2: aIndex out of range
//              -3: aCh out of range


       // *******************************
       // Enable/disable Ftdi latency fix
       // *******************************
//Call this function ***before*** IcpInitCom
//By default the fix is enabled. It works as follows (when enabled):
//- IcpInitCom reads latency of FTDI COM
//- if latency is 1 or 2 then nothing is done
//- if latency >2 then latency 1 is tried to be forced and future operations
//  with ICP2 are not allowed until it becomes <=2
//Disabling is required for multi-thread applications (like Softlog FTMs)
void DLL_FUNC IcpEnableFtdiLatencyFix(int aEnable); //0=disable, 1=enable


       // **********************************
       // For internal Softlog use (testers)
       // **********************************
//General use of function 26
//--------------------------
int      DLL_FUNC IcpFunc26Gen( int aCom,                 //command
                                unsigned char *aTxData,   //Tx data
                                unsigned char aTxQuan,    //number of data bytes for Tx
                                unsigned char *aRxData);   //Rx data
//Parameter:
//aCom: command
//aTxData: Tx buffer, size = FTB9_BUF_SIZE (32 on non-G3, 195 on G3)
//aTxQuan: number of data bytes to be sent, should not exceed FTB9_BUF_SIZE
//aRxData: Rx buffer (received data), size must be >= FTB9_BUF_SIZE
//Return value: according to AUTO_ERROR_LEVEL


/*
Use IcpFunc26Gen() for direct UART communication with UUT as follows (w/o RAM buffer
separate write/read, but RAM buffer is used internally by the firmware
during comUART_DIRECT):
- aCom:    comUART_DIRECT
- aTxData: unsigned char TxBuf[32] (32=FTB9_BUF_SIZE)
           [0]-communication type according to enum UART_ACTION, usually UART_TX_RX
           [1]-pure number of bytes for Tx with UUT (TxPureQuan)
           [2]...[31]-data to be sent to UUT
- aTxQuan: total number of data bytes to be transferred from PC to FTB9
           (TxPureQuan+2)
- aRxData: unsigned char RxBuf[32] (32=FTB9_BUF_SIZE)
           [0]-number of received bytes
           [1]...[31]-data from UUT
*/

void      DLL_FUNC IcpFtb9DsblCompCons(int aDsbl);
//Disables or enables(default) function 33 (read computed constants) in flow
//of function 26. May be useful for faster communication with UUT
// ***IMPORTANT***: execute at least 1 communication with enabled
// "read computed constants" for proper communication settings

//Internal functions for Softlog internal PC-driven applications
//--------------------------------------------------------------
void DLL_FUNC IcpFtb9LedsUnchanged(int aUnchanged);
int  DLL_FUNC IcpPowerOn(void);
int  DLL_FUNC IcpPowerOff(void);
int  DLL_FUNC IcpSendPar(void);
int  DLL_FUNC IcpShowPassFailResult(void);

//Firmware Upgrade
//----------------
// Note:
// After calling IcpFirmwareUpgrade() or IcpActivateOptions()
// the sequence { IcpEndApplication(); IcpStartApplication(); } must be executed.
int  DLL_FUNC IcpFirmwareUpgrade (const char* aHexFile, const char* aCfgFile);
int  DLL_FUNC IcpActivateOptions (const char* aHexFile, const char* aCfgFile);

//Misc. functions
//---------------
int  DLL_FUNC IcpGetSecurityBit(int* aValue);
int  DLL_FUNC IcpSaveHexFile (const char *aFileHex);
void DLL_FUNC IcpGetSecureCounterRange(unsigned* aMin, unsigned* aMax, unsigned* aUnlimited);
int  DLL_FUNC IcpSaveConfigFile(const char* aFileName);
int  DLL_FUNC IcpGetGangType(void);
int  DLL_FUNC IcpSetGangType(int aGangType);

//ICP2-Portable Counter and ENV button lock (preset/get only)
//-----------------------------------------------------------
int  DLL_FUNC IcpSetPortableCntLim(unsigned int aEnv, unsigned int aCntLim); //sets counter, returns 0 if OK
int  DLL_FUNC IcpSetPortableEnvButLock(unsigned int aLock); //lock ENV button
int  DLL_FUNC IcpGetPortableLastEnv(void);

#ifdef __cplusplus
  }
#endif

#pragma pack()

#endif // C_ICPEXP_H


